#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/gpio.h>
#include <linux/printk.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <plat/dmtimer.h>
#include <linux/err.h>
#include <linux/clk.h>

#define OMAP3PWMBL_MAX_BRIGHT 255
#define OMAP3PWMBL_DEF_BRIGHT 255

static int frequency = 200;
module_param(frequency, int, S_IRUGO);
MODULE_PARM_DESC(frequency, "PWM frequency in Hz (default: 200)");

static int brightness = OMAP3PWMBL_DEF_BRIGHT;
module_param(brightness, int, S_IRUGO);
MODULE_PARM_DESC(brightness, "PWM brightness (default: 100)");

static int timer_id = 8;
module_param(timer_id, int, S_IRUGO);
MODULE_PARM_DESC(timer_id, "ID of the OMAP timer to use (default: 8)");

static int duty_min = 0;
module_param(duty_min, int, S_IRUGO);
MODULE_PARM_DESC(duty_min, "Minimum duty cycle in % (default: 0)");

static int duty_max = 100;
module_param(duty_max, int, S_IRUGO);
MODULE_PARM_DESC(duty_max, "Maximum duty cycle in % (default: 100)");

static int round_up = 0;
module_param(round_up, int, S_IRUGO);
MODULE_PARM_DESC(round_up, "From the maximum value minus this one the driver will round to maximum (default: 0)");

static int round_down = 0;
module_param(round_down, int, S_IRUGO);
MODULE_PARM_DESC(round_down, "From the minimum value plus this one the driver will round to minimum (default: 0)");

struct omap_dm_timer* timer;
u32 input_freq;
u32 tldr;
u32 tmar;

extern void omap_mux_set_gpio(u16 val, int gpio);
static int omap3pwmbl_update_status(struct backlight_device *bl)
{
	int tmp_brightness;
	brightness = bl->props.brightness;

	tmp_brightness = brightness;
 
        /* Set the pin in PWM mode */
        omap_mux_set_gpio(3,58);

	if (bl->props.power != FB_BLANK_UNBLANK || bl->props.fb_blank != FB_BLANK_UNBLANK)
		tmp_brightness = 0;
	else  // rescale brightness from {duty_min}% to {duty_max}%
	{
		if(tmp_brightness - round_down < 0)
			tmp_brightness = 0;
		if(tmp_brightness + round_up > OMAP3PWMBL_MAX_BRIGHT)
			tmp_brightness = OMAP3PWMBL_MAX_BRIGHT;
		tmp_brightness = tmp_brightness * tmp_brightness / OMAP3PWMBL_MAX_BRIGHT;
		tmp_brightness = tmp_brightness * (duty_max-duty_min) / 100 + duty_min * OMAP3PWMBL_MAX_BRIGHT / 100;
	}

	tmar = tldr + input_freq / frequency * tmp_brightness / OMAP3PWMBL_MAX_BRIGHT;
	omap_dm_timer_set_match(timer, 1, tmar);

	return 0;
}

static int omap3pwmbl_get_brightness(struct backlight_device *bl)
{
	return brightness;
}

static const struct backlight_ops softpwmbl_ops = {
	.update_status	= omap3pwmbl_update_status,
	.get_brightness	= omap3pwmbl_get_brightness,
};

static int omap3pwmbl_probe(struct platform_device *dev)
{
	int err;
	struct clk *fclk;
	struct backlight_device *bl;
	struct backlight_properties props;

	printk("OMAP3 PWM Backlight: Loading OMAP3 PWM backlight driver\n");

	// init omap hardware timer
	timer = omap_dm_timer_request_specific(timer_id);
	if(!timer)
	{
		printk(KERN_ERR "OMAP3 PWM Backlight: Unable to get the OMAP DM timer with ID %d!\n", timer_id);
		return -EBUSY;
	}

	omap_dm_timer_set_pwm(timer, 0, 1, OMAP_TIMER_TRIGGER_OVERFLOW_AND_COMPARE);

	err = omap_dm_timer_set_source(timer, OMAP_TIMER_SRC_SYS_CLK);
	if(err)
	{
		printk(KERN_ERR "OMAP3 PWM Backlight: Unable to set clock source for timer: %d\n", err);
		return err;
	}
	fclk = omap_dm_timer_get_fclk(timer);
	input_freq = clk_get_rate(fclk);
	tldr = 0xFFFFFFFF - input_freq / frequency - 1;
	tmar = tldr + input_freq / frequency;
	omap_dm_timer_set_load(timer, 1, tldr);
	omap_dm_timer_set_match(timer, 1, tmar);
	omap_dm_timer_start(timer);

	memset(&props, 0, sizeof(struct backlight_properties));
	props.type = BACKLIGHT_RAW;
	props.max_brightness = OMAP3PWMBL_MAX_BRIGHT;
	bl = backlight_device_register(dev->name, &dev->dev, NULL, &softpwmbl_ops, &props);
	if (IS_ERR(bl))
	{
		printk(KERN_ERR "OMAP3 PWM Backlight: Unable to register backlight device!\n");
		omap_dm_timer_free(timer);
		return PTR_ERR(bl);
	}

	bl->props.brightness = brightness;

	platform_set_drvdata(dev, bl);

	printk(KERN_INFO "OMAP3 PWM Backlight: Brightness value range: %d-%d\n", 0, OMAP3PWMBL_MAX_BRIGHT);
	printk(KERN_INFO "OMAP3 PWM Backlight: Brightness value range with rounds: %d-%d\n", round_down, OMAP3PWMBL_MAX_BRIGHT-round_up);
	printk(KERN_INFO "OMAP3 PWM Backlight: Duty cycle range: %d%%-%d%%\n", duty_min, duty_max);
	printk(KERN_INFO "OMAP3 PWM Backlight: Frequency: %dHz\n", frequency);
	printk(KERN_INFO "OMAP3 PWM Backlight: OMAP timer ID: %d\n", timer_id);

	return 0;
}

static int omap3pwmbl_remove(struct platform_device *dev)
{
	struct backlight_device *bl = platform_get_drvdata(dev);

	backlight_device_unregister(bl);
	platform_set_drvdata(dev, NULL);

	omap_dm_timer_free(timer);
	return 0;
}

static struct platform_driver softpwmbl_driver = {
	.driver		= {
		.name	= "omap3-pwm-bl",
	},
	.remove		= omap3pwmbl_remove,
};

static struct platform_device atmel_pwm_bl_dev = {
        .name           = "omap3-pwm-bl",
        .id             = 0,
        .dev            = {
                .platform_data = NULL,
        },
};

static int __init start_module(void)
{
	int err;

	// We register the divice right here in the driver.
	// Normally this should be done in the board setup
	// file. So move the platform_device_register(),
	// omap_pwm_bl_data and atmel_pwm_bl_dev to the
	// board set up file.
	err = platform_device_register(&atmel_pwm_bl_dev);
	if(err)
	{
		printk(KERN_ERR "OMAP3 PWM Backlight: Unable to register platform device!\n");
		return err;
	}

	err = platform_driver_probe(&softpwmbl_driver, omap3pwmbl_probe);
	if(err)
	{
		printk(KERN_ERR "OMAP3 PWM Backlight: Unable to register platform driver!\n");
		return err;
	}

	return 0;
}

static void __exit stop_module(void)
{
	platform_driver_unregister(&softpwmbl_driver);
	printk("OMAP3 PWM Backlight: OMAP3 PWM backlight driver unloaded\n");
	return;
}

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Anders <dx-support@andersdx.com>");
MODULE_DESCRIPTION("OMAP 3 PWM based backlight driver");
module_init(start_module);
module_exit(stop_module);

