/*
 * scf0403853.c -- support for Data Image scf0403853-04 LCD
 *
 * Copyright (c) 2012 Anders Electronics plc. All Rights Reserved.
 *
 *  Inspired by Alberto Panizzo <maramaopercheseimorto@gmail.com> &
 * 	Marek Vasut work in l4f00242t03.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/lcd.h>
#include <linux/slab.h>
#include <linux/regulator/consumer.h>

#include <linux/spi/spi.h>
#include <linux/spi/scf0403852-04.h>

#include <linux/module.h>
#include <linux/fs.h>

#ifdef SCF_DEBUG_DRIVER
#define SCF_MAJOR 123
#define BUF_SIZE 32
static u8 _read_buffer[BUF_SIZE];
static int _last_read_len = 0;
static struct spi_device* _spi = NULL;
#endif

#define param(x) ((x) | 0x100)

struct scf0403853_priv {
	struct spi_device	*spi;
	struct lcd_device	*ld;
	int lcd_state;
	struct regulator *io_reg;
	struct regulator *core_reg;
};

static void scf0403853_reset(unsigned int gpio)
{
	pr_err("scf0403853_reset.\n");
	gpio_set_value(gpio, 1);
	mdelay(100);
	gpio_set_value(gpio, 0);
	mdelay(40);	/* tRES >= 100us */
	gpio_set_value(gpio, 1);
	mdelay(100);
}

static int scf0403853_spi_transfer(struct spi_device *spi, int cmd, const u8 *wbuf,
			   int wlen, u8 *rbuf, int rlen)
{
	struct spi_message	m;
	struct spi_transfer	*x, xfer[4];
	u16			w;
	int			r = 0;

	spi_message_init(&m);

	memset(xfer, 0, sizeof(xfer));
	x = &xfer[0];

	cmd &=  0xff;
	x->tx_buf		= &cmd;
	x->bits_per_word	= 9;
	x->len			= 2;
	spi_message_add_tail(x, &m);

	if (wlen) {
		x++;
		x->tx_buf		= wbuf;
		x->len			= wlen;
		x->bits_per_word	= 9;
		spi_message_add_tail(x, &m);
	}

	if (rlen) {
		x++;
		x->rx_buf	= &w;
		x->len		= 1;
		spi_message_add_tail(x, &m);

		if (rlen > 1) {
			/* Arrange for the extra clock before the first
			 * data bit.
			 */
			x->bits_per_word = 9;
			x->len		 = 2;

			x++;
			x->rx_buf	 = &rbuf[1];
			x->len		 = rlen - 1;
			spi_message_add_tail(x, &m);
		}
	}

	r = spi_sync(spi, &m);

	if (rlen)
		rbuf[0] = w & 0xff;

	return r;
}

static void scf0403853_lcd_init(struct spi_device *spi)
{
	struct scf0403853_pdata *pdata = spi->dev.platform_data;

        const u8 extc = 0xff;
	const u16 extcParam[] = { param(0xff), param(0x98), param(0x06) };

        const u8 spiinterfacetype = 0xba;
        const u16 spiinterfacetypeParam[] = {param(0x60)};

        const u8 bc = 0xbc;
        const u16 bcParam[] = {param(0x01), param(0x10), param(0x61), param(0x74), param(0x01), param(0x01), param(0x1B), param(0x12),
                               param(0x71), param(0x00), param(0x00), param(0x00), param(0x01), param(0x01), param(0x05), param(0x00),
                               param(0xFF), param(0xF2), param(0x01), param(0x00), param(0x40),}; 

        const u8 bd = 0xbd;
        const u16 bdParam[] = {param(0x01), param(0x23), param(0x45), param(0x67), param(0x01), param(0x23), param(0x45), param(0x67)};

        const u8 be = 0xbe;
        const u16 beParam[] = {param(0x01), param(0x22), param(0x22), param(0xBA), param(0xDC), param(0x26), param(0x28), param(0x22), param(0x22)};

        const u8 vCom = 0xc7;
        const u16 vComParam[] = {param(0x74)};

        const u8 vMesurementset = 0xed;
        const u16 vMesurementsetParam[] = {param(0x7F), param(0x0F), param(0x00)};
    
        const u8 powerControl = 0xc0;
        const u16 powerControlParam[] = {param(0x03), param(0x0b), param(0x00)};

        const u8 lvglVoltage = 0xfc;
        const u16 lvglVoltageParam[] = {param(0x08)};
  
        const u8 displayFunction = 0xb6;
        const u16 displayFunctionParam[] = {param(0xa0)};

        const u8 engineeringSetting = 0xdf;
        const u16 engineeringSettingParam[] = {param(0x00), param(0x00), param(0x00), param(0x00), param(0x00), param(0x20)};

        const u8 dvddvoltageSetting = 0xf3;
        const u16 dvddvoltageSettingParam[] = {param(0x74)};

        const u8 displayInversion = 0xb4;
        const u16 displayInversionParam[] = {param(0x00), param(0x00), param(0x00)};

        const u8 panelResolution = 0xf7;
        const u16 panelResolutionParam[] = {param(0x82)};

	const u8 frameRate = 0xb1;
        const u16 frameRateParam[] = {param(0x00), param(0x13), param(0x13)};

        const u8 panelTiming = 0xf2;
        const u16 panelTimingParam[] = {param(0x80), param(0x05), param(0x40), param(0x28)};

        const u8 powerControl2 = 0xc1;
        const u16 powerControl2Param[] = {param(0x17), param(0x75), param(0x79), param(0x20)};

        const u8 memoryAccess = 0x36;
        const u16 memoryAccessParam[] = {param(0x00)};

        const u8 pixelFormat = 0x3a;
        const u16 pixelFormatParam[] = {0x66};

        const u8 pgammaControl = 0xe0;
        const u16 pgammaControlParam[] = {param(0x00), param(0x03), param(0x0b), param(0x0c), param(0x0e), param(0x08), param(0xc5), param(0x04),
                                     param(0x08), param(0x0c), param(0x13), param(0x11), param(0x11), param(0x14), param(0x0c), param(0x10)};

        const u8 ngammaControl = 0xe1;
        const u16 ngammaControlParam[] = {param(0x00), param(0x0d), param(0x11), param(0x0c), param(0x0c), param(0x04), param(0x76), param(0x03),
                                     param(0x08), param(0x0b), param(0x16), param(0x10), param(0x0d), param(0x16), param(0x0a), param(0x00)};

       
        const u8 tearingEffect = 0x35;
        const u16 tearingEffectParam[] = {param(0x00)};  

        dev_err(&spi->dev, "initializing LCD\n");

	// reset LCD 
	scf0403853_reset(pdata->reset_gpio);



        if (scf0403853_spi_transfer(spi, extc, (const u8 *)extcParam, sizeof(extcParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, spiinterfacetype, (const u8 *)spiinterfacetypeParam, sizeof(spiinterfacetypeParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}


        if (scf0403853_spi_transfer(spi, bc, (const u8 *)bcParam, sizeof(bcParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, bd, (const u8 *)bdParam, sizeof(bdParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, be, (const u8 *)beParam, sizeof(beParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, vCom, (const u8 *)vComParam, sizeof(vComParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, vMesurementset, (const u8 *)vMesurementsetParam, sizeof(vMesurementsetParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, powerControl, (const u8 *)powerControlParam, sizeof(powerControlParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, lvglVoltage, (const u8 *)lvglVoltageParam, sizeof(lvglVoltageParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, displayFunction, (const u8 *)displayFunctionParam, sizeof(displayFunctionParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        
        if (scf0403853_spi_transfer(spi, engineeringSetting, (const u8 *)engineeringSettingParam, sizeof(engineeringSettingParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, dvddvoltageSetting, (const u8 *)dvddvoltageSettingParam, sizeof(dvddvoltageSettingParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, displayInversion, (const u8 *)displayInversionParam, sizeof(displayInversionParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, panelResolution, (const u8 *)panelResolutionParam, sizeof(panelResolutionParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, frameRate, (const u8 *)frameRateParam, sizeof(frameRateParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, panelTiming, (const u8 *)panelTimingParam, sizeof(panelTimingParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, powerControl2, (const u8 *)powerControl2Param, sizeof(powerControl2Param), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, memoryAccess, (const u8 *)memoryAccessParam, sizeof(memoryAccessParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, pixelFormat, (const u8 *)pixelFormatParam, sizeof(pixelFormatParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, pgammaControl, (const u8 *)pgammaControlParam, sizeof(pgammaControlParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, ngammaControl, (const u8 *)ngammaControlParam, sizeof(ngammaControlParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}

        if (scf0403853_spi_transfer(spi, tearingEffect, (const u8 *)tearingEffectParam, sizeof(tearingEffectParam), NULL, 0) < 0) {
		pr_err("Setting display function - failed");
	}
}

static void scf0403853_lcd_powerdown(struct spi_device *spi)
{
	struct scf0403853_pdata *pdata = spi->dev.platform_data;
	struct scf0403853_priv *priv = dev_get_drvdata(&spi->dev);

	dev_err(&spi->dev, "Powering down LCD\n");

	gpio_set_value(pdata->data_enable_gpio, 0);

	if (priv->io_reg)
		regulator_disable(priv->io_reg);

	if (priv->core_reg)
		regulator_disable(priv->core_reg);
}

static int scf0403853_lcd_power_get(struct lcd_device *ld)
{
	struct scf0403853_priv *priv = lcd_get_data(ld);

	return priv->lcd_state;
}

static int scf0403853_lcd_power_set(struct lcd_device *ld, int power)
{
	struct scf0403853_priv *priv = lcd_get_data(ld);
	struct spi_device *spi = priv->spi;

	const u16 slpout = 0x11;
	const u16 dison = 0x29;

	const u16 slpin = 0x10;
	const u16 disoff = 0x28;

	dev_dbg(&spi->dev, "LCD Power set, power = %d, curr power = %d\n", power, priv->lcd_state);

	if (power <= FB_BLANK_NORMAL) {
		if (priv->lcd_state <= FB_BLANK_NORMAL) {
			dev_dbg(&spi->dev, "LCD sleep in-out\n");

		} else if (priv->lcd_state < FB_BLANK_POWERDOWN) {
			dev_dbg(&spi->dev, "Resuming LCD\n");
			scf0403853_spi_transfer(spi, slpout, NULL, 0, NULL, 0);
			msleep(120);
			scf0403853_spi_transfer(spi, dison, NULL, 0, NULL, 0);
		} else {
			/* priv->lcd_state == FB_BLANK_POWERDOWN */
			scf0403853_lcd_init(spi);
			priv->lcd_state = FB_BLANK_VSYNC_SUSPEND;
			scf0403853_lcd_power_set(priv->ld, power);
		}
	} else if (power < FB_BLANK_POWERDOWN) {
		if (priv->lcd_state <= FB_BLANK_NORMAL) {
			/* Send the display in standby */
			dev_info(&spi->dev, "Standby the LCD\n");

			scf0403853_spi_transfer(spi, disoff, NULL, 0, NULL, 0);
			msleep(60);
			scf0403853_spi_transfer(spi, slpin, NULL, 0, NULL, 0);
		} else if (priv->lcd_state < FB_BLANK_POWERDOWN) {
			/* Do nothing, the LCD is already in standby */
		} else {
			/* priv->lcd_state == FB_BLANK_POWERDOWN */
			scf0403853_lcd_init(spi);
			priv->lcd_state = FB_BLANK_UNBLANK;
			scf0403853_lcd_power_set(ld, power);
		}
	} else {
		/* power == FB_BLANK_POWERDOWN */
		if (priv->lcd_state != FB_BLANK_POWERDOWN) {
			/* Clear the screen before shutting down */
			scf0403853_spi_transfer(spi, disoff, NULL, 0, NULL, 0);
			msleep(60);
			scf0403853_lcd_powerdown(spi);
		}
	}

	priv->lcd_state = power;

	return 0;
}

static struct lcd_ops l4f_ops = {
	.set_power	= scf0403853_lcd_power_set,
	.get_power	= scf0403853_lcd_power_get,
};

static int __devinit scf0403853_probe(struct spi_device *spi)
{
	struct scf0403853_priv *priv;
	struct scf0403853_pdata *pdata = spi->dev.platform_data;
	int ret;

	dev_info(&spi->dev, "LCD probe.\n");

	if (pdata == NULL) {
		dev_err(&spi->dev, "Uninitialized platform data.\n");
		return -EINVAL;
	}

	priv = kzalloc(sizeof(struct scf0403853_priv), GFP_KERNEL);

	if (priv == NULL) {
		dev_err(&spi->dev, "No memory for this device.\n");
		return -ENOMEM;
	}

	dev_set_drvdata(&spi->dev, priv);
	spi->bits_per_word = 8;
	spi_setup(spi);

	priv->spi = spi;

	priv->ld = lcd_device_register("scf0403853-04", &spi->dev, priv, &l4f_ops);
	if (IS_ERR(priv->ld)) {
		ret = PTR_ERR(priv->ld);
		goto err;
	}

	/* Init the LCD */
	scf0403853_lcd_init(spi);
	priv->lcd_state = FB_BLANK_VSYNC_SUSPEND;
	scf0403853_lcd_power_set(priv->ld, FB_BLANK_UNBLANK);

	dev_info(&spi->dev, "DataImage scf0403853 lcd probed.\n");

#ifdef SCF_DEBUG_DRIVER
	_spi = spi;
#endif

	return 0;

err:
	kfree(priv);

	return ret;
}

static int __devexit scf0403853_remove(struct spi_device *spi)
{
	struct scf0403853_priv *priv = dev_get_drvdata(&spi->dev);
	struct scf0403853_pdata *pdata = priv->spi->dev.platform_data;

	scf0403853_lcd_power_set(priv->ld, FB_BLANK_POWERDOWN);
	lcd_device_unregister(priv->ld);

	dev_set_drvdata(&spi->dev, NULL);

	gpio_free(pdata->data_enable_gpio);
	gpio_free(pdata->reset_gpio);

	if (priv->io_reg)
		regulator_put(priv->io_reg);
	if (priv->core_reg)
		regulator_put(priv->core_reg);

	kfree(priv);

	return 0;
}

static void scf0403853_shutdown(struct spi_device *spi)
{
	struct scf0403853_priv *priv = dev_get_drvdata(&spi->dev);

	if (priv)
		scf0403853_lcd_power_set(priv->ld, FB_BLANK_POWERDOWN);

}

#ifdef SCF_DEBUG_DRIVER
static int scf_open(struct inode *inode, struct file *file)
{
	printk("scf_open: successful\n");
	return 0;
}

static int scf_release(struct inode *inode, struct file *file)
{
    printk("scf_release: successful\n");
    return 0;
}

static ssize_t scf_read(struct file *file, char *buf, size_t count, loff_t *ptr)
{
    if (_last_read_len == 0) {
    	return 0;
    }
    if (count != _last_read_len) {
    	return -1;
    }

    return count;
}

static ssize_t scf_write(struct file *file, const char *buf, size_t count, loff_t * ppos)
{
    int is_read;
    int buf_len;
    int cmd;
    int ret = 0;
    int i;
    u16 write_params[BUF_SIZE];

    if (count < 3) {
    	return -1;
    }

    /* write command */
    is_read = buf[0];
    buf_len = buf[1];
    cmd = buf[2];
    if (is_read) {
    	_last_read_len = 0;
    	ret = scf0403853_spi_transfer(_spi, cmd, NULL, 0, _read_buffer, buf_len);
    	if (ret >= 0) {
    		_last_read_len = buf_len;
    	}
    }
    else {
    	for (i = 0; i < buf_len; i++) {
    		write_params[i] = 0x100 | buf[i + 3];
    	}
    	ret = scf0403853_spi_transfer(_spi, cmd, (const u8*)write_params, buf_len * 2, NULL, 0);
    	if (ret >= 0) {
    		_last_read_len = buf_len;
    	}
    }

    return 0;
}
#endif

static struct spi_driver scf0403853_driver = {
	.driver = {
		.name	= "scf0403853-04",
		.owner	= THIS_MODULE,
	},
	.probe		= scf0403853_probe,
	.remove		= __devexit_p(scf0403853_remove),
	.shutdown	= scf0403853_shutdown,
};

#ifdef SCF_DEBUG_DRIVER
struct file_operations scf_fops = {
    owner:   THIS_MODULE,
    read:    scf_read,
    write:   scf_write,
    open:    scf_open,
    release: scf_release,
};
#endif

static __init int scf0403853_init(void)
{
#ifdef SCF_DEBUG_DRIVER
    if (register_chrdev(SCF_MAJOR, "scf", &scf_fops) < 0) {
        printk("Error registering scf device\n");
    }
#endif
	return spi_register_driver(&scf0403853_driver);
}

static __exit void scf0403853_exit(void)
{
	spi_unregister_driver(&scf0403853_driver);
}

module_init(scf0403853_init);
module_exit(scf0403853_exit);

MODULE_DESCRIPTION("DataImage scf0403853 LCD");
MODULE_LICENSE("GPL v2");

