/*
 * Touch Screen driver for DataImage's I2C connected touch screen panels
 *   Copyright (c) 2012 Anders Electronics
 *
 * Based on migor_ts.c
 *   Copyright (c) 2008 Magnus Damm
 *   Copyright (c) 2007 Ujjwal Pande <ujjwal@kenati.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU  General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/pm.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <linux/i2c.h>
#include <linux/timer.h>

#define EVENT_PENDOWN 1
#define EVENT_REPEAT  2
#define EVENT_PENUP   3


#ifdef CONFIG_LCD_SCF0403
#define MAX_X 480
#define MAX_Y 800
#define FINGERS 4
#endif

#ifdef CONFIG_LCD_SCF0403852_03
#define MAX_X 480
#define MAX_Y 272
#define FINGERS 2  
#endif

#define FINGER_SIZE 4

struct di_ts_priv {
	struct i2c_client *client;
	struct input_dev *input[FINGERS];   
	struct delayed_work work;
	char phys[2][32];
	char name[2][32];
	int irq;
	int opened;
	int added;
};

static int di_ts_open(struct input_dev *dev);
static void di_ts_close(struct input_dev *dev);

static void di_ts_add_last_input(struct di_ts_priv *priv)
{
	struct input_dev *input[FINGERS];
	int i = 0;

	input[i] = input_allocate_device();
	if (!input[i]) {
		dev_err(&priv->client->dev, "Failed to allocate input device.\n");
		return;
	}

	input[i]->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS) | BIT_MASK(EV_SYN);
	input[i]->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
	input_set_abs_params(input[i], ABS_X, 0, MAX_X, 0, 0);
	input_set_abs_params(input[i], ABS_Y, 0, MAX_Y, 0, 0);
	input_set_abs_params(input[i], ABS_PRESSURE, 0, 0xff, 0, 0);

	snprintf(priv->phys[i], sizeof(priv->phys[i]), "%s/input%d", priv->client->name, i);
	snprintf(priv->name[i], sizeof(priv->name[i]), "%s%d", priv->client->name, i);

	input[i]->phys = priv->phys[i];
	input[i]->name = priv->name[i];
	input[i]->id.bustype = BUS_I2C;
	input[i]->dev.parent = &priv->client->dev;

	input[i]->open = di_ts_open;
	input[i]->close = di_ts_close;

	input_set_drvdata(input[i], priv);
	if (input_register_device(input[i]))
				return;

	priv->input[i] = input[i];
}

static void di_ts_poscheck(struct work_struct *work)
{
	struct di_ts_priv *priv = container_of(work,
						  struct di_ts_priv,
						  work.work);
	const u8 NO_EVENT = 0xFF;
	unsigned short i = 0, xpos, ypos;
	u8 getEvent = 0x85; 
       

        #ifdef CONFIG_LCD_SCF0403
        u8 buf[FINGERS*FINGER_SIZE + 16];
        #endif
        
        #ifdef CONFIG_LCD_SCF0403852_03
	u8 buf[16];
        #endif

       

	if (!priv->added) {
		di_ts_add_last_input(priv);
		priv->added = 1;
		goto out;
	}

	memset(buf, 0, sizeof(buf));

	if (i2c_master_send(priv->client, &getEvent, 1) != 1) {
		dev_err(&priv->client->dev, "Unable to write i2c\n");
		goto out;
	}

	/* Now do Page Read */
	if (i2c_master_recv(priv->client, buf, sizeof(buf)) != sizeof(buf)) {
		dev_err(&priv->client->dev, "Unable to read data  from i2c\n");
		goto out;
	}

	for (i = 0; i < FINGERS; i++)
	{
		dev_dbg(&priv->client->dev, "Buffer [%d]: %02X %02X %02X %02X\n",
				i, buf[i * FINGER_SIZE], buf[i * FINGER_SIZE + 1], buf[i * FINGER_SIZE + 2], buf[i * FINGER_SIZE + 3]);

		if (buf[i * FINGER_SIZE] == NO_EVENT)
		{
                        dev_dbg(&priv->client->dev, "Untouched %d", i);

			input_report_key(priv->input[i], BTN_TOUCH, 0);
			input_report_abs(priv->input[i], ABS_PRESSURE, 0);
			input_sync(priv->input[i]);
		}
		else 
		{
                     

                        #ifdef CONFIG_LCD_SCF0403
                        xpos = buf[i * FINGER_SIZE + 1] | (buf[ i * FINGER_SIZE + 0] << 8);
                        ypos = buf[i * FINGER_SIZE + 3] | (buf[ i * FINGER_SIZE + 2] << 8);
			#endif

                        #ifdef CONFIG_LCD_SCF0403852_03
			xpos = buf[i * FINGER_SIZE + 1] | (buf[i * FINGER_SIZE + 0] << 8);
                        ypos = buf[i * FINGER_SIZE + 3] | (buf[i * FINGER_SIZE + 2] << 8);
			#endif

	
			dev_dbg(&priv->client->dev, "Touched %d: x = %d, y = %d\n", i, xpos, ypos);
			input_report_key(priv->input[i], BTN_TOUCH, 1);
			input_report_abs(priv->input[i], ABS_PRESSURE, 0xFF);
			input_report_abs(priv->input[i], ABS_X, xpos);
			input_report_abs(priv->input[i], ABS_Y, ypos);
			input_sync(priv->input[i]);
		}
	}

out:

	enable_irq(priv->irq);
}

static irqreturn_t di_ts_isr(int irq, void *dev_id)
{
	struct di_ts_priv *priv = dev_id;

	/* the touch screen controller chip is hooked up to the cpu
	 * using i2c and a single interrupt line. the interrupt line
	 * is pulled low whenever someone taps the screen. to deassert
	 * the interrupt line we need to acknowledge the interrupt by
	 * communicating with the controller over the slow i2c bus.
	 *
	 * we can't acknowledge from interrupt context since the i2c
	 * bus controller may sleep, so we just disable the interrupt
	 * here and handle the acknowledge using delayed work.
	 */
	disable_irq_nosync(irq);
	schedule_delayed_work(&priv->work, HZ / 100);

	return IRQ_HANDLED;
}

static int di_ts_open(struct input_dev *dev)
{
	struct di_ts_priv *priv = input_get_drvdata(dev);
	if (priv->opened) {
		return 0;
	}
	priv->opened = 1;

	pr_debug("--> opening 'scf0403_ts'\n");


	schedule_delayed_work(&priv->work, HZ);

	return 0;
}

static void di_ts_close(struct input_dev *dev)
{
	struct di_ts_priv *priv = input_get_drvdata(dev);

	disable_irq(priv->irq);

	/* cancel pending work and wait for di_ts_poscheck() to finish */
	if (cancel_delayed_work(&priv->work)) {
		/*
		 * if di_ts_poscheck was canceled we need to enable IRQ
		 * here to balance disable done in di_ts_isr.
		 */
		enable_irq(priv->irq);
	}
}

static int di_ts_probe(struct i2c_client *client,
			  const struct i2c_device_id *idp)
{
	struct di_ts_priv *priv;
	struct input_dev *input[FINGERS];
	int error, i;
   
	char sleepOut = 0x81;
	char senseOn = 0x83;
	char clearStack = 0x88;
	char getDevId = 0x31;
	char speedMode[] = { 0x9D, 0x80 };
	char mcuTurnOn[] = { 0x35, 0x02 };
	char getSleep = 0x63;
        char buf[4];

               
	char flashTurnOn[] = {0x36, 0x0F, 0x53};
        char fetchflashMode[] = {0xDD, 0x04, 0x02};
 
        char reloadDisable[] = { 0x42, 0x02};
        char setCacheFunc[] = {0xDD, 0x06, 0x02};
        char setCvdd[] = {0x76, 0x01, 0x2D};
           

	priv = kzalloc(sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&client->dev, "failed to allocate driver data\n");
		error = -ENOMEM;
		goto err0;
	}

	dev_set_drvdata(&client->dev, priv);

	/* register all inputs except the first one */
	for (i = 1; i < FINGERS; i++) {
		input[i] = input_allocate_device();
		if (!input[i]) {
			dev_err(&client->dev, "Failed to allocate input device.\n");
			error = -ENOMEM;
			goto err1;
		}

		input[i]->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS) | BIT_MASK(EV_SYN);
		input[i]->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
		input_set_abs_params(input[i], ABS_X, 0, MAX_X, 0, 0);
		input_set_abs_params(input[i], ABS_Y, 0, MAX_Y, 0, 0);
		input_set_abs_params(input[i], ABS_PRESSURE, 0, 0xff, 0, 0);

		snprintf(priv->phys[i], sizeof(priv->phys[i]), "%s/input%d", client->name, i);
		snprintf(priv->name[i], sizeof(priv->name[i]), "%s%d", client->name, i);

		input[i]->phys = priv->phys[i];
		input[i]->name = priv->name[i];
		input[i]->id.bustype = BUS_I2C;
		input[i]->dev.parent = &client->dev;

		input[i]->open = di_ts_open;
		input[i]->close = di_ts_close;

		input_set_drvdata(input[i], priv);

		error = input_register_device(input[i]);
		if (error)
			goto err1;

		priv->input[i] = input[i];
	}

	priv->client = client;
	INIT_DELAYED_WORK(&priv->work, di_ts_poscheck);
	priv->irq = client->irq;
	priv->opened = 0;
	priv->added = 0;

	error = request_irq(priv->irq, di_ts_isr, IRQF_TRIGGER_LOW,
			    client->name, priv);
	if (error) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		goto err2;
	}

	disable_irq(priv->irq);

	device_init_wakeup(&client->dev, 1);

        
      
	// init touch controller 
	if (i2c_master_send(client, &getSleep, 1) != 1)
	{
		pr_err("Unable to send get sleep command\n");
	}
	else
	{
		i2c_master_recv(client, buf, 1);
		pr_info("reg[%02X] = %02X", getSleep, buf[0]);
		printk(" Anders Touch reg[%02X] = %02X", getSleep, buf[0]);
	}

	// initialize the controller if it's not initialized 
	if (buf[0] == 0)
	{
                /*
		if (i2c_master_send(client, &sleepOut, 1) != 1) {
			dev_err(&client->dev, "Unable to sleep out\n");
		}

		msleep(120);

                

		if (i2c_master_send(client, mcuTurnOn, 2) != 2) {
			dev_err(&client->dev, "Unable to turn on the MCU\n");
		}

                
                msleep(10);
                if (i2c_master_send(client, flashTurnOn, 3) != 3) {
			dev_err(&client->dev, "Unable to turn on the flash\n");
		}
                msleep(10);
                if (i2c_master_send(client, fetchflashMode, 3) != 3) {
			dev_err(&client->dev, "Unable to turn on the fetchFlashmode\n");
		}
                msleep(10);
                if (i2c_master_send(client, &senseOn, 1) != 1) {
			dev_err(&client->dev, "Unable to sense on\n");
		}
                msleep(100);
                */

                if (i2c_master_send(client, reloadDisable, 2) != 2) {
			dev_err(&client->dev, "Unable to sleep out\n");
		}

                if (i2c_master_send(client, mcuTurnOn, 2) != 2) {
			dev_err(&client->dev, "Unable to turn on the MCU\n");
		}

                msleep(10);

                if (i2c_master_send(client, flashTurnOn, 3) != 3) {
			dev_err(&client->dev, "Unable to turn on the flash\n");
		}

                msleep(10);

                if (i2c_master_send(client, setCacheFunc, 3) != 3) {
			dev_err(&client->dev, "Unable to turn on the flash\n");
		}

                msleep(10);

                if (i2c_master_send(client, setCvdd, 3) != 3) {
			dev_err(&client->dev, "Unable to turn on the flash\n");
		}

                msleep(10);

                
                if (i2c_master_send(client, &senseOn, 1) != 1) {
			dev_err(&client->dev, "Unable to sense on\n");
		}

                msleep(120);

                if (i2c_master_send(client, &sleepOut, 1) != 1) {
			dev_err(&client->dev, "Unable to sense on\n");
		} 
                
                
		// Read the events once to arm the IRQ 
		if (i2c_master_send(client, &getDevId, 1) != 1) {
			dev_err(&client->dev, "Unable get device ID\n");
		}
		else
		{
			i2c_master_recv(client, buf, 3);
			pr_info("Device ID: 0x%02X%02X%02X\n", buf[0], buf[1], buf[2]);
		}

	}

	return 0;

 err2:
    for (i = FINGERS - 1; i > 0; i--) {
		input_unregister_device(input[i]);
		input[i] = NULL;
    }
 err1:
	for ( ; i >= 0; i--) {
		input_free_device(input[i]);
	}
	kfree(priv);
 err0:
	dev_set_drvdata(&client->dev, NULL);
	return error;
}

static int di_ts_remove(struct i2c_client *client)
{
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);
	int i;

	free_irq(priv->irq, priv);
	for (i = FINGERS - 1; i > 0; i--) {
		input_unregister_device(priv->input[i]);
	}
	kfree(priv);

	dev_set_drvdata(&client->dev, NULL);

	return 0;
}

static int di_ts_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);

	if (device_may_wakeup(&client->dev))
		enable_irq_wake(priv->irq);

	return 0;
}

static int di_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);

	if (device_may_wakeup(&client->dev))
		disable_irq_wake(priv->irq);

	return 0;
}

static SIMPLE_DEV_PM_OPS(di_ts_pm, di_ts_suspend, di_ts_resume);

static const struct i2c_device_id di_ts_id[] = {
	{ "hx8520-c", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, di_ts);

static struct i2c_driver di_ts_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "hx8520-c",
		.pm = &di_ts_pm,
	},
	.probe = di_ts_probe,
	.remove = di_ts_remove,
	.id_table = di_ts_id,
};

static int __init di_ts_init(void)
{
	return i2c_add_driver(&di_ts_driver);
}

static void __exit di_ts_exit(void)
{
	i2c_del_driver(&di_ts_driver);
}

MODULE_DESCRIPTION("DataImage Touchscreen driver");
MODULE_LICENSE("GPL");

module_init(di_ts_init);
module_exit(di_ts_exit);

