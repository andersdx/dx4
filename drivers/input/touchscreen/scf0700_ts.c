/*
 * Touch Screen driver for DataImage's I2C connected touch screen panels
 *   Copyright (c) 2012 Anders Electronics
 *
 * Based on migor_ts.c
 *   Copyright (c) 2008 Magnus Damm
 *   Copyright (c) 2007 Ujjwal Pande <ujjwal@kenati.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU  General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/pm.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <linux/i2c.h>
#include <linux/timer.h>
#include <linux/delay.h>

#ifdef CONFIG_LCD_SCF0700C48
#define FINGERS 5
#define REG_NUM 5
#define PRESSURE_START 27
#else
#define FINGERS 2
#define REG_NUM 4
#define TOLERANCE 20
#endif

struct di_ts_priv {
	struct i2c_client *client;
	struct input_dev *input[FINGERS];
	struct delayed_work work;
	struct delayed_work init;
	char phys[FINGERS][32];
	char name[FINGERS][32];
	int irq;
	int opened;
	int prev_finger_count;
	int enable;
};

static int di_ts_open(struct input_dev *dev);
static void di_ts_close(struct input_dev *dev);

static int di_ts_add_input(struct di_ts_priv *priv, int i)
{
	struct input_dev *input;
	int error;

	input = input_allocate_device();
	if (!input) {
		dev_err(&priv->client->dev, "Failed to allocate input device.\n");
		return -ENOMEM;
	}
	priv->input[i] = input;

	input->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS) | BIT_MASK(EV_SYN);
	input->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
	input_set_abs_params(input, ABS_X, 0, 800, 0, 0);
	input_set_abs_params(input, ABS_Y, 0, 480, 0, 0);
	input_set_abs_params(input, ABS_PRESSURE, 0, 0xff, 0, 0);

	snprintf(priv->phys[i], sizeof(priv->phys[i]), "%s/input%d", priv->client->name, i);
	snprintf(priv->name[i], sizeof(priv->name[i]), "%s%d", priv->client->name, i);

	input->phys = priv->phys[i];
	input->name = priv->name[i];
	input->id.bustype = BUS_I2C;
	input->dev.parent = &priv->client->dev;

	input->open = di_ts_open;
	input->close = di_ts_close;

	input_set_drvdata(input, priv);

	error = input_register_device(input);
	if (error)
		return error;

	input = input_allocate_device();
	if (!input) {
		dev_err(&priv->client->dev, "Failed to allocate input device.\n");
		return -ENOMEM;
	}

	if (priv->enable) {
		enable_irq(priv->irq);
	}

	return 0;
}


#ifdef CONFIG_LCD_SCF0700C48
static void di_ts_poscheck(struct work_struct *work)
{
	struct di_ts_priv *priv = container_of(work,
						  struct di_ts_priv,
						  work.work);
	static char ids[FINGERS] = {-1, -1, -1, -1, -1};
	short xpos, ypos;
	char used_dev[FINGERS] = {0, 0, 0, 0, 0};
	char treated[FINGERS] = {0, 0, 0, 0, 0};
	int finger_count;
	int finger;
	char id;
	int missed_untouched = 0;
	int i, j, res = 0;
	u8 buf[32];

	/* Set Index 0 */
	buf[0] = 0;
	res = i2c_master_send(priv->client, buf, 1);
	if (res != 1) {
		dev_err(&priv->client->dev, "Unable to write i2c, error: %d\n", res);
		goto out;
	}

	/* Now do Page Read */
	if (i2c_master_recv(priv->client, buf, sizeof(buf)) != sizeof(buf)) {
		dev_err(&priv->client->dev, "Unable to read i2c page\n");
		goto out;
	}

	finger_count = buf[0] & 0x07;

	/* find all input devices used in the previous event */
	for (i = 0; i < FINGERS; i++) {
		id = buf[i * REG_NUM + 6];
		for (j = 0; j < FINGERS; j++) {
			if (id == ids[j]) {
				used_dev[j] = 1;
				break;
			}
		}
	}

	/* send touched finger events */
	for (finger = 0; finger < finger_count; finger++) {
		id = buf[finger * REG_NUM + 6];

		if (id == 0) {
			// invalid ID
			continue;
		}

		xpos = buf[finger * REG_NUM + 2] | (buf[finger * REG_NUM + 3] << 8);
		ypos = buf[finger * REG_NUM + 4] | (buf[finger * REG_NUM + 5] << 8);

		/* find index of the input device used in the previous event */
		for (j = 0; j < FINGERS; j++) {
			if (ids[j] == id) {
				break;
			}
		}

		/* if this input device was not used in previous event find first unused input device */
		if (j >= FINGERS) {
			for (j = 0; j < FINGERS; j++) {
				if (!used_dev[j]) {
					used_dev[j] = 1;
					ids[j] = id;
					break;
				}
			}
		}

		if (j >= FINGERS)
		{
			/* error? */
			continue;
		}

		treated[j] = 1;
		input_report_abs(priv->input[j], ABS_PRESSURE, 0xFF);
		input_report_abs(priv->input[j], ABS_X, xpos);
		input_report_abs(priv->input[j], ABS_Y, ypos);
		input_report_key(priv->input[j], BTN_TOUCH, 1);
		input_sync(priv->input[j]);
		dev_dbg(&priv->client->dev, "Dev = %d, id = %d x = %d, y = %d\n", j, id, xpos, ypos);
	}

	/* send untouched finger events - find in IDs that was touched in the previous interrupt */
	for (finger = finger_count; finger < priv->prev_finger_count; finger++) {

		/* find an ID used by CTP */
		for (j = 0; j < FINGERS; j++) {
			/* search the ID in the unused stack slots */
			for (i = finger_count; i < FINGERS; i++ ) {
				if (ids[j] == buf[i * REG_NUM + 6]) {
					/* The ID was found - mark it as unused */
					id = ids[j];
					ids[j] = -1;
					used_dev[j] = 0;
					break;
				}
			}
			if (i < FINGERS) {
				/* The ID is found */
				break;
			}
		}

		if (j >= FINGERS)
		{
			/* the ID was not found */
			missed_untouched++;
			continue;
		}

		treated[j] = 1;
		dev_dbg(&priv->client->dev, "Off dev = %d, id = %d\n", j, id);
		input_report_abs(priv->input[j], ABS_PRESSURE, 0);
		input_report_key(priv->input[j], BTN_TOUCH, 0);
		input_sync(priv->input[j]);
	}

	if (missed_untouched > 0) {
		/* send all untouched that was missed */
		for (finger = 0; finger < missed_untouched; finger++) {
			/* search the IDs used in previous but not send */
			for (j = 0; j < FINGERS; j++ ) {
				if (!treated[j]) {
					/* The ID was found - mark it as unused */
					ids[j] = -1;
					used_dev[j] = 0;
					treated[j] = 1;
					break;
				}
			}
			if (j >= FINGERS)
			{
				/* error? */
				continue;
			}

			dev_dbg(&priv->client->dev, "Off dev = %d\n", j);
			input_report_abs(priv->input[j], ABS_PRESSURE, 0);
			input_report_key(priv->input[j], BTN_TOUCH, 0);
			input_sync(priv->input[j]);
		}
	}

	/* reset IDs of unused devices */
	for (j = 0; j < FINGERS; j++) {
		if (!used_dev[j]) {
			ids[j] = -1;
		}
	}
	priv->prev_finger_count = finger_count;

out:
	enable_irq(priv->irq);
}
#else
static void di_ts_poscheck(struct work_struct *work)
{
	struct di_ts_priv *priv = container_of(work,
						  struct di_ts_priv,
						  work.work);
	short xpos, ypos;
	int res = 0;
	int finger;
	int finger_count;
	u8 buf[10];
	int i;

	/* read the data twice to partially workaround missing interrupts */
	for (i = 0; i < 2; i++)
	{
		/* Set Index 0 */
		buf[0] = 0;
		res = i2c_master_send(priv->client, buf, 1);
		if (res != 1) {
			dev_err(&priv->client->dev, "Unable to write i2c, error: %d\n", res);
			goto out;
		}

		/* Now do Page Read */
		if (i2c_master_recv(priv->client, buf, sizeof(buf)) != sizeof(buf)) {
			dev_err(&priv->client->dev, "Unable to read i2c page\n");
			goto out;
		}

		finger_count = buf[0];

		for (finger = 0; finger < finger_count; finger++) {
			xpos = buf[finger * REG_NUM + 2] | (buf[finger * REG_NUM + 3] << 8);
			ypos = buf[finger * REG_NUM + 4] | (buf[finger * REG_NUM + 5] << 8);

			input_report_abs(priv->input[finger], ABS_PRESSURE, 0xFF);
			input_report_abs(priv->input[finger], ABS_X, xpos);
			input_report_abs(priv->input[finger], ABS_Y, ypos);
			input_report_key(priv->input[finger], BTN_TOUCH, 1);
			input_sync(priv->input[finger]);
			dev_dbg(&priv->client->dev, "Dev = %d, x = %d, y = %d\n", finger, xpos, ypos);
		}

		for (finger = priv->prev_finger_count - 1; finger >= finger_count; finger--) {
			dev_dbg(&priv->client->dev, "Off dev = %d\n", finger);
			input_report_abs(priv->input[finger], ABS_PRESSURE, 0);
			input_report_key(priv->input[finger], BTN_TOUCH, 0);
			input_sync(priv->input[finger]);
		}
		priv->prev_finger_count = finger_count;
	}

out:
	enable_irq(priv->irq);
	return;
}
#endif

static void di_ts_add_last_input(struct work_struct *work)
{
	struct di_ts_priv *priv = container_of(work,
						  struct di_ts_priv,
						  init.work);

	di_ts_add_input(priv, 0);
}

static irqreturn_t di_ts_isr(int irq, void *dev_id)
{
	struct di_ts_priv *priv = dev_id;

	/* the touch screen controller chip is hooked up to the cpu
	 * using i2c and a single interrupt line. the interrupt line
	 * is pulled low whenever someone taps the screen. to deassert
	 * the interrupt line we need to acknowledge the interrupt by
	 * communicating with the controller over the slow i2c bus.
	 *
	 * we can't acknowledge from interrupt context since the i2c
	 * bus controller may sleep, so we just disable the interrupt
	 * here and handle the acknowledge using delayed work.
	 */

	disable_irq_nosync(irq);
	schedule_delayed_work(&priv->work, HZ / 100);

	return IRQ_HANDLED;
}


static int di_ts_open(struct input_dev *dev)
{
	struct di_ts_priv *priv = input_get_drvdata(dev);
#ifdef CONFIG_LCD_SCF0700C48
	char buf[] = { 0x34, 0x0B };
#else
	char buf[] = { 0x15, 0x09 };
#endif
	char read;
	int res;

	if (priv->opened) {
		return 0;
	}
	priv->opened = 1;
	pr_err("--> opening 'di_scf0700_ts', irq = %d\n", priv->irq);

	res = i2c_master_send(priv->client, buf, 2);
	if (res != 2) {
		dev_err(&priv->client->dev, "Unable to init i2c, error: %d\n", res);
	}

	res = i2c_master_send(priv->client, buf, 1);
	if (res != 1) {
		dev_err(&priv->client->dev, "Unable to init i2c, error: %d\n", res);
	}

	res = i2c_master_recv(priv->client, &read, 1);
	if (res != 1) {
		dev_err(&priv->client->dev, "Unable to read: %d\n", res);
	}
	pr_err("INT_MODE register: %02X\n", read);


	/* Read the events once to arm the IRQ */
	//di_ts_poscheck(&priv->work.work);
	//enable_irq(priv->irq);

	/* init the last input */
	priv->enable = 1;
	schedule_delayed_work(&priv->init, HZ);

	return 0;
}

static void di_ts_close(struct input_dev *dev)
{
	struct di_ts_priv *priv = input_get_drvdata(dev);

	if (!priv->opened) {
		return;
	}
	priv->opened = 0;

	disable_irq(priv->irq);

	/* cancel pending work and wait for di_ts_poscheck() to finish */
	if (cancel_delayed_work_sync(&priv->work)) {
		/*
		 * if di_ts_poscheck was canceled we need to enable IRQ
		 * here to balance disable done in di_ts_isr.
		 */
		enable_irq(priv->irq);
	}
}

static int di_ts_probe(struct i2c_client *client,
			  const struct i2c_device_id *idp)
{
	struct di_ts_priv *priv;
	int error, i = 0;

	/* allocate memory for private state structure */
	priv = kzalloc(sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&client->dev, "failed to allocate driver data\n");
		error = -ENOMEM;
		goto err0;
	}

	/* bind the private state to the device */
	dev_set_drvdata(&client->dev, priv);
	priv->client = client;
	priv->enable = 0;

	/* init virtual input devices devices according to number of
	 * supported fingers, except the first one, which will be
	 * initialized after the first device opening */
	for (i = FINGERS - 1; i >= 1; i--) {
		error = di_ts_add_input(priv, i);
		if (error) {
			goto err1;
		}
	}

	/* init the private structure with delayed function pointers and initial state values */
	INIT_DELAYED_WORK(&priv->work, di_ts_poscheck);
	INIT_DELAYED_WORK(&priv->init, di_ts_add_last_input);
	priv->irq = client->irq;
	priv->opened = 0;
	priv->prev_finger_count = 0;

	/* register the interrupt procedure */
	error = request_irq(priv->irq, di_ts_isr, IRQF_TRIGGER_FALLING, client->name, priv);
	if (error) {
		dev_err(&client->dev, "Unable to request touchscreen IRQ.\n");
		goto err2;
	}

	/* disable the IRQ until the first device usage */
	disable_irq(priv->irq);

	device_init_wakeup(&client->dev, 1);
	return 0;

 err2:
	for (i = FINGERS - 1 ; i >= 0; i--) {
		input_unregister_device(priv->input[i]);
		priv->input[i] = NULL; /* so we dont try to free it below */
	}
 err1:
	for ( ; i >= 0; i--) {
	    input_free_device(priv->input[i]);
	}
	kfree(priv);
 err0:
	dev_set_drvdata(&client->dev, NULL);
	return error;
}

static int di_ts_remove(struct i2c_client *client)
{
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);
	int i;

	free_irq(priv->irq, priv);
	for (i = 0 ; i < FINGERS; i++) {
		input_unregister_device(priv->input[i]);
	}
	kfree(priv);

	dev_set_drvdata(&client->dev, NULL);

	return 0;
}

static int di_ts_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);

	if (device_may_wakeup(&client->dev))
		enable_irq_wake(priv->irq);

	return 0;
}

static int di_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct di_ts_priv *priv = dev_get_drvdata(&client->dev);

	if (device_may_wakeup(&client->dev))
		disable_irq_wake(priv->irq);

	return 0;
}

static SIMPLE_DEV_PM_OPS(di_ts_pm, di_ts_suspend, di_ts_resume);

static const struct i2c_device_id di_ts_id[] = {
	{ "scf0700_ts", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, di_ts);

static struct i2c_driver di_ts_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "scf0700_ts",
		.pm = &di_ts_pm,
	},
	.probe = di_ts_probe,
	.remove = di_ts_remove,
	.id_table = di_ts_id,
};

static int __init di_ts_init(void)
{
	return i2c_add_driver(&di_ts_driver);
}

static void __exit di_ts_exit(void)
{
	i2c_del_driver(&di_ts_driver);
}

MODULE_DESCRIPTION("DataImage Touchscreen driver");
MODULE_LICENSE("GPL");

module_init(di_ts_init);
module_exit(di_ts_exit);

